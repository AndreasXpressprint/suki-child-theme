<?php
/**
 * Child Theme main functions file.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Candidate
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;


// Enqueue Child Theme CSS using priority 20 so parent theme CSS is loaded first
function candidate_enqueue_styles() {
    wp_enqueue_style(
        'candidate',
        get_stylesheet_uri(),
        array( 'suki' ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'candidate_enqueue_styles', 20 );


// Create CPT "Candidate"
function create_posttype() {
    register_post_type( 'candidate',
        array(
            'labels' => array(
                'name' => __( 'Candidates' ),
                'singular_name' => __( 'Candidate' )
            ),
            'supports' => array('title', 'editor', 'thumbnail'),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'candidate'),
            'show_in_rest' => true,
 
        )
    );
}
add_action( 'init', 'create_posttype' );


// Disable various feeds

// Redirect to homepage if trying to access feeds directly
function disable_feeds() {
	wp_redirect( home_url() );
	die;
}

// Disable the feeds
add_action( 'do_feed',      'disable_feeds', -1 );
add_action( 'do_feed_rdf',  'disable_feeds', -1 );
add_action( 'do_feed_rss',  'disable_feeds', -1 );
add_action( 'do_feed_rss2', 'disable_feeds', -1 );
add_action( 'do_feed_atom', 'disable_feeds', -1 );
add_action( 'do_feed_rss2_comments', 'disable_feeds', -1 );
add_action( 'do_feed_atom_comments', 'disable_feeds', -1 );

// Prevent feed links from being inserted in page source
add_action( 'feed_links_show_posts_feed',    '__return_false', -1 );
add_action( 'feed_links_show_comments_feed', '__return_false', -1 );
remove_action( 'wp_head', 'feed_links',       2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );

